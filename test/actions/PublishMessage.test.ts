import test, { ExecutionContext } from "ava";
import { PublishMessage } from "../../src/actions/PublishMessage";
import { InMemoryTimelines } from "../../src/infrastructure/InMemoryTimelines";

test("Publish a message", (t: ExecutionContext) => {
  const timelines = new InMemoryTimelines();
  const publishMessage = new PublishMessage(timelines);

  publishMessage.execute("Alice", "algo");

  t.true(timelines.get("Alice").includes("algo"));
});

test("Get empty timeline", (t: ExecutionContext) => {
  const timelines = new InMemoryTimelines();

  t.is(timelines.get("Alice").length, 0);
});

test("Publish many messages", (t: ExecutionContext) => {
  const timelines = new InMemoryTimelines();
  const publishMessage = new PublishMessage(timelines);
  publishMessage.execute("Alice", "algo");

  publishMessage.execute("Alice", "otro");

  t.true(timelines.get("Alice").includes("algo"));
  t.true(timelines.get("Alice").includes("otro"));
});

test("Publish many messages with many users", (t: ExecutionContext) => {
  const timelines = new InMemoryTimelines();
  const publishMessage = new PublishMessage(timelines);
  publishMessage.execute("Alice", "algo");

  publishMessage.execute("Bob", "otro");

  t.true(timelines.get("Alice").includes("algo"));
  t.true(timelines.get("Bob").includes("otro"));
});
