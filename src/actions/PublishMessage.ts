import { InMemoryTimelines } from "../infrastructure/InMemoryTimelines";

export class PublishMessage {
  constructor(private timelines: InMemoryTimelines) {}

  execute(user: string, message: string) {
    this.timelines.add(user, message);
  }
}
