export interface Timelines {
  get(name: string): string[];

  add(user: string, message: string): void;
}
