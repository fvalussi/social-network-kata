import { Timelines } from "../domain/Timelines";

export class InMemoryTimelines implements Timelines {
  private timelines: Map<string, string[]> = new Map();

  get(name: string): string[] {
    return this.timelines.get(name) || [];
  }

  add(user: string, message: string) {
    const userTimeline = this.timelines.get(user);
    if (userTimeline) {
      userTimeline.push(message);
    } else {
      this.timelines.set(user, [message]);
    }
  }
}
